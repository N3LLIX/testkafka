FROM golang:1.21.0-alpine3.18

WORKDIR /app

COPY . . 

RUN go mod download

RUN go build -o ./bin/test_kafka ./cmd

CMD ["./bin/test_kafka"]