package main

import (
	"Kafka/pkg/kafka"
	"Kafka/pkg/nats"
	"fmt"
	"os"
	"time"
)

func testKafka(network, address, topic string, partition int) error {
	client := &kafka.KafkaClient{
		Network:   network,
		Address:   address,
		Topic:     topic,
		Partition: partition,
	}

	fmt.Println("Connecting to Kafka broker on " + address)
	for i := 0; i < 3; i++ {
		err := client.Connect()
		if err == nil {
			break
		} else if i == 2 {
			return fmt.Errorf("could not connect: %s", err.Error())

		} else {
			time.Sleep(time.Duration(i) * time.Second)
		}
	}

	defer func() {
		fmt.Println("Disconnecting from Kafka")
		err := client.Disconnect()
		if err != nil {
			fmt.Fprintf(os.Stderr, "error while closing connection: %s\n", err.Error())
			os.Exit(1)
		}
	}()

	fmt.Println("Publishing message")
	err := client.Publish("hello,", "world!")
	if err != nil {
		return fmt.Errorf("could not publish message: %s", err.Error())
	}

	fmt.Println("Reading message back")
	key, value, err := client.Consume()
	if err != nil {
		return fmt.Errorf("could not read message: %s", err.Error())
	}

	fmt.Println(string(key), string(value))
	return nil
}

func testNats(address string) error {
	client := &nats.NatsClient{
		Address: address,
	}

	for i := 0; i < 3; i++ {
		err := client.Connect()
		if err == nil {
			break
		} else if i == 2 {
			return fmt.Errorf("could not connect: %s", err.Error())
		} else {
			time.Sleep(time.Duration(i) * time.Second)
		}
	}

	defer func() {
		err := client.Disconnect()
		if err != nil {
			fmt.Fprintf(os.Stderr, "error while closing connection: %s\n", err.Error())
			os.Exit(1)
		}
	}()

	done := 0
	maxIter := 15

	err := client.Subscribe("test_subject", func(subject string, message []byte) error {
		fmt.Println(string(subject), string(message))
		done++
		return nil
	})
	if err != nil {
		return fmt.Errorf("could not subscribe to topic: %s", err.Error())
	}

	for i := 0; i < maxIter; i++ {
		err = client.Publish("test_subject", fmt.Sprintf("Hi! This is message #%d", i))
		if err != nil {
			return fmt.Errorf("could not publish message: %s", err.Error())
		}
	}

	for maxIter != done {
		time.Sleep(100 * time.Millisecond)
	}

	return nil
}

func main() {
	fmt.Println("Starting test on Kafka:")
	err := testKafka("tcp", "kafka.platform.svc.cluster.local:9092", "test", 0)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Kafka test failed: %s\n", err.Error())
	} else {
		fmt.Println("Kafka test passed!")
	}
	fmt.Println("#--------------------------#")
	fmt.Println("Starting test on Nats:")
	err = testNats("34.147.73.73:32335")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Nats test failed: %s\n", err.Error())
	} else {
		fmt.Println("Nats test passed!")
	}
}
