package pkg

type Connector interface {
	Connect() error
	Disconnect() error
}

type Subscriber interface {
	Connector
	// Subscribe
	// callback function's arguments are the message's subject and data
	Subscribe(string, func(string, string) error) error
	Unsubscribe() error
}

type Producer interface {
	Connector
	Produce(string, string) error
}

type Consumer interface {
	Connector
	Consume() ([]byte, []byte, error)
}

type MessageRead interface {
	Connector
	Read() ([]byte, []byte, error)
}

type MessageWrite interface {
	Connector
	Write([]byte, []byte) error
}
