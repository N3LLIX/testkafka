package kafka

import (
	"context"

	"github.com/segmentio/kafka-go"
)

type KafkaClient struct {
	conn      *kafka.Conn
	Network   string
	Address   string
	Topic     string
	Partition int
}

/* -------------------------------------------------------------------------- */
/*                                  connector                                 */
/* -------------------------------------------------------------------------- */

func (c *KafkaClient) Connect() error {
	if c.conn != nil {
		return nil
	}

	var err error

	c.conn, err = kafka.DialLeader(context.Background(), c.Network, c.Address, c.Topic, c.Partition)

	return err
}

func (c *KafkaClient) Disconnect() error {
	if c.conn == nil {
		return nil
	}

	return c.conn.Close()
}

/* -------------------------------------------------------------------------- */
/*                                message write                               */
/* -------------------------------------------------------------------------- */

func (c *KafkaClient) Write(key, value string) error {
	_, err := c.conn.WriteMessages(kafka.Message{
		Topic:     c.Topic,
		Partition: c.Partition,
		Key:       []byte(key),
		Value:     []byte(value),
	})

	return err
}

/* -------------------------------------------------------------------------- */
/*                                message read                                */
/* -------------------------------------------------------------------------- */

func (c *KafkaClient) Read() ([]byte, []byte, error) {
	message, err := c.conn.ReadMessage(10e3)
	if err != nil {
		return nil, nil, err
	}

	return message.Key, message.Value, nil
}

/* -------------------------------------------------------------------------- */
/*                                  publisher                                 */
/* -------------------------------------------------------------------------- */

func (c *KafkaClient) Publish(key, value string) error {
	return c.Write(key, value)
}

/* -------------------------------------------------------------------------- */
/*                                  consumer                                  */
/* -------------------------------------------------------------------------- */

func (c *KafkaClient) Consume() ([]byte, []byte, error) {
	return c.Read()
}
