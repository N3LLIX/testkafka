package nats

import nats "github.com/nats-io/nats.go"

type NatsClient struct {
	conn    *nats.Conn
	sub     *nats.Subscription
	Address string
}

/* -------------------------------------------------------------------------- */
/*                                  connector                                 */
/* -------------------------------------------------------------------------- */

func (c *NatsClient) Connect() error {
	if c.conn != nil {
		return nil
	}

	var err error

	c.conn, err = nats.Connect(c.Address)

	return err
}

func (c *NatsClient) Disconnect() error {
	if c.conn == nil {
		return nil
	}

	c.conn.Close()

	return nil
}

/* -------------------------------------------------------------------------- */
/*                                message write                               */
/* -------------------------------------------------------------------------- */

func (c *NatsClient) Write(key, value []byte) error {
	return c.conn.Publish(string(key), value)
}

/* -------------------------------------------------------------------------- */
/*                                  publisher                                 */
/* -------------------------------------------------------------------------- */

func (c *NatsClient) Publish(subject, data string) error {
	return c.Write([]byte(subject), []byte(data))
}

/* -------------------------------------------------------------------------- */
/*                                 subscriber                                 */
/* -------------------------------------------------------------------------- */

func (c *NatsClient) Subscribe(subject string, callback func(string, []byte) error) error {
	if c.sub != nil {
		return nil
	}

	var err error

	c.sub, err = c.conn.Subscribe(subject, func(m *nats.Msg) {
		callback(m.Subject, m.Data)
	})
	return err
}

func (c *NatsClient) Unsubscribe() error {
	if c.sub == nil {
		return nil
	}

	return c.sub.Unsubscribe()
}
