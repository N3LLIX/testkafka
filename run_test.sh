if [ -Z $1 ]; then
    echo "Usage: run_test.sh <kafka | nats>"
    exit 1
fi

run_test() {
    if [ $1 = "kafka" ]; then
        docker compose -f docker-compose-kafka.yml up
    elif [ $1 = "nats" ]; then
        docker compose -f docker-compose-nats.yml up
    fi
}


run_test $@